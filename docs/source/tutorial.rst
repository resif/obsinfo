.. _Tutorial:

***************
Tutorial
***************

This tutorial takes you step by step through creating, validating and
using subnetwork and instrumentation files. 

.. toctree::
  :caption:  Table of contents
  :maxdepth: 1

  tutorial/1_subnetwork
  tutorial/2_instrumentation
  tutorial/5_instrument_component
  tutorial/7_stages
  tutorial/8_filter
  tutorial/9_summary
