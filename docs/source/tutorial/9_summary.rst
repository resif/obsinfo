
.. _Tutorial9:

*********************************
Summary
*********************************

As a summary, remember *obsinfo* strives for reuse and flexibility.

#. Try to use the `$ref` as much as possible.
   This allows reuse across files and different campaigns.
#. Start from template files, keeping only necessary fields and fields you need
#. You can add notes at any level. You can add extras only in network, station
   and instrumentation files. Only extras and comments will be reflected as comments in StationXML.
#. Use the ``default`` channel in your ``instrumentation_base`` file to avoid repeating
   redundant information.
#. Use of configuration_definitions to specify variations on the base definition

==================
Conclusion
==================

This finishes our tutorial.
For more detailed information please review the :ref:`Schemas` files, which
define all of the possible elements in information files.
Have fun using *obsinfo*!

If you find any issues or have any questions please use the Issues
functionality of gitlab: https://www.gitlab.com/resif/smm/obsinfo/issues

