.. _Tutorial4:

************************************************************
Building an instrumentation_base file
************************************************************

Instrumentation files represent a complete instrumentation, with one or more
channels consisting of a sensor, a preamplifier and possible a datalogger.
The ``_base`` part of the filetype indicates that this file can have
configurations and can be modified from higher levels.  Instrumentation
files can call lower-level files in order to avoid repeating information.
The figure below compares the hierarchy of files beneath the instrumentation_base
file with the StationXML element hierarchy:

.. image:: images/instrumentation_obsinfo_stationxml.png

The **obsinfo** hierarchy replicates the information in the StationXML
hierarchy, with the least repeated information possible.
Instead of fully and separately declaring each channel, it declares a ``default``
channel, and then any differences that the other channels have with the default.
Instead of having one list of stages that integrates the sensor, preamplifier and
datalogger, each of these **instrument components** has its own list of stages.
The "InstrumentSensitivity" element is absent in obsinfo, as it can be calculated
from the stages.
The StationXML ``Sensor``, ``Preamplifier`` and ``Datalogger`` elements, which
are simply implementations of the ``Equipment`` element
type, are declared as "sensor:equipment", "preamplifier:equipment" and
"datalogger:equipment" in **obsinfo**.


Creating the instrumentation_base file
________________________________________

The easiest way to create an instrumentation_base file is from a template.
By typing ``obsinfo template instrumentation_base``, you will create the
following file, named ``TEMPLATE.instrumentation_base.yaml``


.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.instrumentation_base.yaml
    :language: yaml
    
The template file points to information files in the example database.
For the following, your DATAPATH should point there
(see :ref:`copy_example_database`)

.. _validating_instrumentation_file:

Validating the instrumentation_base file
________________________________________

Validating your instrumentation file follows the same sequences as
:ref:`validating_subnetwork_file` : 

Run the ``schema`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~~~

Type ``obsinfo schema TEMPLATE.instrumentation_base.yaml``.  The console output should be:

.. code-block:: console

    Validating instrumentation_base file
    Reading {somepath}/TEMPLATE.instrumentation_base.yaml
    schema =   istrumentation_base.schema.json
        Testing instance ...OK
    istrumentation_base test for: {somepath}/TEMPLATE.instrumentation_base.yaml: PASSED

Run the  ``print`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~~

Type ``obsinfo print TEMPLATE.instrumentation_base.yaml``.
The console output should be:

.. code-block:: console

    TEMPLATE.instrumentation_base.yaml: 
        Instrumentation:
            equipment: Equipment:
                type: My Ocean Bottom Seismometer
                description: This is my OBS!
                model: MY_OBS
                manufacturer: My OBS park
                vendor: various
                serial_number: 2014a2
                resource_id: IPGP:2004iepw44
                installation_date: 2024-09-30
                removal_date: 2025-08-31
                calibration_dates: OIDates: 2 OIDates
            channels: Channels: [Channel 03.CDH]


Run the ``plot`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~


Type ``obsinfo plot TEMPLATE.instrumentation_base.yaml``.  The program will plot
the instrument responses for each channel:

.. image:: images/obsinfo_plot_instrumentation_base.png

Since the template file only has one channel, only one response is plotted.

Modifying the instrumentation_base file
________________________________________

Simplifying
~~~~~~~~~~~~~~~~~~~~~~~

Every line between an ``# OPTIONAL {...}`` and its matching ``# END OPTIONAL {...}``
comment lines can be removed.  If we remove all of them from
``TEMPLATE.instrumentation_base.yaml`` we get a greatly simplified file that still
validates, prints and plots (although with different ``print`` and ``plot``
results):

.. code-block:: yaml

    ---
    format_version: "1.0"
    revision:
        authors:
            - {$ref: "persons/EXAMPLE.person.yaml"} 
        date: "2024-09-30" 
    instrumentation_base:
        equipment:
            model: "MY_OBS"
            type: "My Ocean Bottom Seismometer"
            description: "This is my OBS!" 
            manufacturer: "My OBS park"
        channels:
            default:
                datalogger:
                    base: {$ref: "datalogger_bases/EXAMPLE.datalogger_base.yaml"}
                sensor:
                    base: {$ref: "sensor_bases/EXAMPLE_BBSeismometer.sensor_base.yaml"}
            "1":   # One element per instrument channel
                orientation:
                    code: "H"   # The SEED orientation (or sub-source) code for this channel
                    azimuth.deg:
                        value: 0
                    dip.deg:
                        value: -90

.. code-block:: console

    > obsinfo print SIMPLE.instrumentation_base.yaml 
    SIMPLE.instrumentation_base.yaml: 
        Instrumentation:
            equipment: Equipment:
                type: My Ocean Bottom Seismometer
                description: This is my OBS!
                model: MY_OBS
                manufacturer: My OBS park
            channels: Channels: [Channel 00.HHZ]

.. code-block:: console

    > obsinfo plot SIMPLE.instrumentation_base.yaml 

.. image:: images/obsinfo_plot_simple_instrumentation_base.png

The sensor response is different because, in the original template file,
we replaced the seismometer sensor by a differential pressure gauge.  Note that,
in the modified file, the orientation code is now bad (still ``"H"``), we need
to change that to ``"Z"`` under ``orientation:code:``

.. image:: images/obsinfo_plot_simple_instrumentation_base_corrected.png

Adding channels
~~~~~~~~~~~~~~~~~~~~~~~

Below we add some channels, to create a typical OBS with three seismometer
channels and one pressure channel. The new channel section is:

.. code-block:: yaml

    channels:
        default:
            datalogger:
                base: {$ref: "datalogger_bases/EXAMPLE.datalogger_base.yaml"}
            sensor:
                base: {$ref: "sensor_bases/EXAMPLE_BBSeismometer.sensor_base.yaml"}
        "1":
            orientation: {code: "Z", azimuth.deg: {value: 0}, dip.deg: {value: -90}}
        "2":
            orientation: {code: "1", azimuth.deg: {value: 0, uncertainty: 180},
                          dip.deg: {value: -90}}
        "3":
            orientation: {code: "2", azimuth.deg: {value: 90, uncertainty: 180},
                          dip.deg: {value: -90}}
        "4":
            orientation: {code: "H", azimuth.deg: {value: 0}, dip.deg: {value: -90}}
            replace_sensor:
                base: {$ref: "sensor_bases/EXAMPLE_DPG.sensor_base.yaml"}

The ``replace_sensor`` element for channel key=``"4"`` allows us to completely
replace the sensor.  If we had used a ``sensor`` element, it would have simply
modified the existing sensor.
Also note that the channel ``key`` is not the same as its ``code``.  
The key is not seen in the output StationXML file and can be any text string.
We recommend using a code for the instrumentations' internal port, which can be
useful for converting proprietary data to miniSEED format.

The ``print`` and ``plot`` outputs change accordingly:

.. code-block:: console

    MULTCHANNEL.instrumentation_base.yaml: 
        Instrumentation:
            equipment: Equipment:
                type: My Ocean Bottom Seismometer
                description: This is my OBS!
                model: MY_OBS
                manufacturer: My OBS park
            channels: Channels:
                - Channel 00.HHZ
                - Channel 00.HH1
                - Channel 00.HH2
                - Channel 00.HDH

.. image:: images/obsinfo_plot_multichannel_instrumentation_base.png

We only see two traces in each plot, because all of the
seismometer channels have the same response.

Modifying channels
~~~~~~~~~~~~~~~~~~~~~~~

You can use the ``configuration``, ``modification`` and/or ``stage_modification``
elements to modify from the ``instrument components'`` default properties:

.. code-block:: yaml

    channels:
        default:
            datalogger:
                base: {$ref: "datalogger_bases/EXAMPLE.datalogger_base.yaml"}
            sensor:
                base: {$ref: "sensor_bases/EXAMPLE_BBSeismometer.sensor_base.yaml"}
        "1":
            orientation: {code: "Z", azimuth.deg: {value: 0}, dip.deg: {value: -90}}
        "2":
            orientation: {code: "1", azimuth.deg: {value: 0, uncertainty: 180},
                          dip.deg: {value: -90}}
            sensor: {configuration: "SN1-399, differential"}
        "3":
            orientation: {code: "2", azimuth.deg: {value: 90, uncertainty: 180},
                          dip.deg: {value: -90}}
            sensor: {stage_modifications: {"1": {gain: {value: 20}}}}
        "4":
            orientation: {code: "H", azimuth.deg: {value: 0}, dip.deg: {value: -90}}
            replace_sensor:
                base: {$ref: "sensor_bases/EXAMPLE_DPG.sensor_base.yaml"}

This is only advised if the OBS has different default parameters than what was
specified in the **instrument component** files.

For the above case, each channel now has a different gain:

.. image:: images/obsinfo_plot_modified_instrumentation_base.png
