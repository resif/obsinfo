
.. _Tutorial8:

******************************
Building a filter file
******************************

As pointed out in the previous section, all stages have an associated
"filter", even if no filtering is performed.
The filters specified in StationXML are:

* :ref:`poleszeros.filter` - Any kind of digital filter specified by its poles and its zeros. Use this for Laplace transforms and IIR filters.
* :ref:`FIR.filter` - Finite Input Response digital filter
* :ref:`coefficients.filter` - A FIR expressed with coefficients of transfer function
* :ref:`response_list.filter` - A digital filter with all responses expressed as frequency, amplitude and phase.
* :ref:`polynomial.filter` - **Not yet implemented in obspy, so it doesn't work in obsinfo**

These are all available in **obsinfo**.

**obsinfo** also offers a few "simplified" filters for stages which do not
actually have filters, and/or which have a more natural representation than
in the above 5 filters: 

* :ref:`AD_Conversion.filter` - Analog to digital conversion stage
* :ref:`analog.filter` - gain only analog stage
* :ref:`digital.filter`  gain only digital stage

Below we show the template file and its plot for each filter type:

poleszeros.filter
-----------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.poleszeros.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_poleszeros.png

FIR.filter
--------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.poleszeros.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_FIR.png

.. image:: images/obsinfo_plot_filter_FIR_coeffs.png

coefficients.filter
----------------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.coefficients.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_coefficients.png

.. image:: images/obsinfo_plot_filter_coefficients_coeffs.png

response_list.filter
----------------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.response_list.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_response_list.png

Reveals the instability possible if you specify a response list!

polynomial.filter
----------------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.polynomial.filter.yaml
    :language: yaml

**POLYNOMIAL FILTERS ARE NOT IMPLEMENTED IN OBSPY 1.4.0**

AD_Conversion.filter
----------------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.AD_Conversion.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_AD_Conversion.png

.. image:: images/obsinfo_plot_filter_AD_Conversion_coeffs.png

AD_Conversion filters have no frequency response.They are implemented as ``Coefficients``
objects with only one coefficient, so you get the coefficients plot as well.

analog.filter
--------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.analog.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_analog.png

Analog filters are implemented as pole-zero objects with no poles and no zeros.

digital.filter
--------------

.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.digital.filter.yaml
    :language: yaml

.. image:: images/obsinfo_plot_filter_digital.png

.. image:: images/obsinfo_plot_filter_digital_coeffs.png

Looks like the analog filter, but it also shows off its "coefficient"
