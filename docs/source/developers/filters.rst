**************************
Filters
**************************


All of the following classes are subclsses of FilterTemplate:
 - Coefficients
 - FIR
 - PolesZeros
 - ResponseList
 - Polynomial
 - ADCONVERSION
 - ANALOG
 - DIGITAL

They are NOT subclasses of Filter, which exists only to return one of the 
FilterTemplate subclasses.  I therefore use Filter.construct() rather than
Filter()

Filter.construct figures out which filter subclass to create, creates it and
returns it.

The different filter types can be created using Filter.construct(), a
"Factory"  method which
returns an object of one of the Filter subclasses