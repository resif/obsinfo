****************
Tests
****************

Testing minimizes bugs and shows how routines should be called.
Proper tests can avoid the all-to-frequent case of a code modification breaking
previously working code.
In general, every method of a class should have a test and, if you discover
a bug that is not covered by a test, you should add a test for it.
A good practice when you modify a function is to see if that function has
a test code that will see what you are changing and, if not, to add it and
run the test before and after your modifications.

**obsinfo** has two ``test/`` directories: one at the root level that does
fast unit testing, and another at the ``src/obsinfo`` level that tests
every example and template file.  The second is much slower and should not
be run all of the time: but it should be run before each version deployment.
We implement testing using the unittest Unit testing framework
(https://docs.python.org/3/library/unittest.html). 


To run the fast tests, go into the ``{root}/tests/`` directory and run

.. code-block:: bash

    pytest
    
To run the slow tests, go into the ``{root}/src/obsinfo/tests/`` directory and run

.. code-block:: bash

    pytest
    
