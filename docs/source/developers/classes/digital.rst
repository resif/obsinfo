
.. _Digital11:

***************
Digital
***************

===============
 Description
===============

StationXML does not have a class for digital stages which are not filters.
They are therefore implemented as a Coefficients filter with one numerator
coefficient, equal to 1.


---------------------
Python class:
---------------------

Digital

---------------------
 YAML / JSON label:
---------------------

DIGITAL

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Coefficients (with no coefficients)

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`Coefficients <Coefficients11>`

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Is nested in :ref:`Stage <Stage>`

==============================
Attributes
==============================

======================== ================================== ============ ================= ========================== =========================================
        **Name**                   **Type**                 **Required**    **Default**    **Equivalent StationXML**                   **Remarks**
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
*None*
                                                                                             
======================== ================================== ============ ================= ========================== =========================================

==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/filter.schema.json>`_

==============================
Example
==============================

`_templates/digital.filter.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.digital.filter.yaml
  :language: YAML
    
==================
Class Navigation
==================

:ref:`Filter11` \<\=\= 

