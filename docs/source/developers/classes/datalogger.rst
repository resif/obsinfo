
.. _Datalogger11:

*******************
Datalogger
*******************

===============
 Description
===============

A datalogger is the part of an OBS instrument which records the signal after processing.
It is an :ref:`InstrumentComponent <InstrumentComponent>` with response stages
and attributes such as the global delay correction and the overall sample rate of the instrument.

---------------------
Python class:
---------------------

Datalogger

---------------------
 YAML / JSON label:
---------------------

datalogger

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Datalogger

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`InstrumentComponent <InstrumentComponent>`

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Is element of :ref:`Equipment <Equipment>`
* Contains :ref:`Stages <Stages>`

==============================
Attributes
==============================

================== ============== ============ =========== ========================= =============================================================
  **Name**             **Type**   **Required** **Default** **Equivalent StationXML**  **Remarks**
------------------ -------------- ------------ ----------- ------------------------- -------------------------------------------------------------
 sample_rate           number            Y        *None*         *None*    
------------------ -------------- ------------ ----------- ------------------------- -------------------------------------------------------------
 correction            number            N        *None*         *None*              Used by *obsinfo* to calculate StationXML ``correction``
                                                                                     for each stage.  correction will be set to 0 for each stage 
                                                                                     except the last,                                                                                                 which is the value of correction.                    
================== ============== ============ =========== ========================= =============================================================

*For the rest of attributes, see superclass :ref:`InstrumentComponent <InstrumentComponent>`*

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/datalogger_base.schema.json>`_

==============================
Example
==============================

file: `_templates/datalogger_base.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.datalogger_base.yaml
  :language: YAML

==================
Class Navigation
==================

:ref:`InstrumentComponent` \<\=\=\> :ref:`Stages`

           

