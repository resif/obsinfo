.. _PolesZeros11:

************
PolesZeros
************


===============
Description
===============

A Pole-Zero :ref:`filter <Filter11>`. Every digital filter can be specified by its poles and zeros (together with a gain factor). Poles and zeros give useful insights into a filter's response. For a more detailed discussion, `click here <https://ccrma.stanford.edu/~jos/filters/Pole_Zero_Analysis_I.html>`_.

---------------------
Python class:
---------------------

* PolesZeros

---------------------
 YAML / JSON label:
---------------------

* PolesZeros

------------------------------------------
Corresponding StationXML structure
------------------------------------------

No direct correspondence. Mapped into subattribute `PolesZeros <http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#poleszeros>`_ of attribute `Stage <http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#stage>`_.

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`Filter <Filter11>`

-----------------------------------------
Subclasses
-----------------------------------------

* :ref:`Analog <Analog11>`

-----------------------------------------
Relationships
-----------------------------------------

* Is nested in :ref:`Stage <Stage>`

==============================
Attributes
==============================

.. _PZTransferFunctionType: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#pztransferfunctiontype-required
.. _Zero: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#zero
.. _Pole: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#pole
.. _NormalizationFrequency: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#normalizationfrequency-required
.. _NormalizationFactor: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#normalizationfactor-required
.. _`More info...`: https://ccrma.stanford.edu/~jos/filters/Transfer_Function_Analysis.html

======================== =============================== ============ ================= ========================== =========================================
        **Name**                   **Type**              **Required**    **Default**     **Equivalent StationXML**                   **Remarks**
------------------------ ------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
transfer_function_type    List of Values:                     N       LAPLACE           `PzTransferFunctionType`_     `More info...`_ 
                           LAPLACE (RADIANS/SECOND),                  (RADIANS/SECOND)
                           LAPLACE (HERTZ),                                            
                           DIGITAL (Z-TRANFORM)
------------------------ ------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
zeros                    List of numbers                      Y              *None*       `Zero`_
------------------------ ------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
poles                    List of numbers                      Y              *None*        `Pole`_
------------------------ ------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
normalization_frequency  number                               N            *None*       `NormalizationFrequency`_
------------------------ ------------------------------- ------------ ----------------- -------------------------- ----------------------------------------- 
normalization_factor     number                               N             *None*      `NormalizationFactor`_      Frequency at which the 
                                                                                                                    NormalizationFactor is valid. 
                                                                                                                    This should be the same for all stages
                                                                                                                    and within the passband, if any.

======================== =============================== ============ ================= ========================== =========================================

 
==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/filter.schema.json>`_

==============================
Example
==============================

`_templates/TEMPLATE.poleszeros.filter.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.poleszeros.filter.yaml
  :language: YAML

==================
Class Navigation
==================

:ref:`Filter11` \<\=\= 

