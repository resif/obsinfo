
.. _FDSNNetwork:

********************
 Network
********************

===============
 Description
===============

FDSN network information about the  :ref:`SubNetwork <SubNetwork>`.

---------------------
Python class:
---------------------

Network

---------------------
 YAML / JSON label:
---------------------

network

------------------------------------------
Corresponding StationXML structure
------------------------------------------

*None*
Individual attributes in this class belong to the Network attribute.

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

*None*

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Is part of the specification of a :ref:`SubNetwork <SubNetwork>`

==============================
Attributes
==============================

.. _code: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#network-required
.. _process: http://docs.fdsn.org/projects/source-identifiers/en/v1.0/network-codes.html
.. _Description: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#description 
.. _Comment: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#comment 
.. _startDate: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#network-required
.. _endDate: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#network-required

======================== ================================== ============ ================= =============================== =============================================================
        **Name**                   **Type**                 **Required**    **Default**    **Equivalent StationXML**          **Remarks**
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -------------------------------------------------------------
  code                             string                          Y           *None*             `code`_                     Codes are assigned by FDSN according to this `process`_
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -------------------------------------------------------------
  name                             string                          Y           *None*            *None*                       Will be added as a `Comment`_                   
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -------------------------------------------------------------
  description                      string                          Y           *None*             `Description`_              Not required in StationXML
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -------------------------------------------------------------
  comment                          string                          N           *None*                `Comment`_
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -------------------------------------------------------------
  start_date                        date                           Y           *None*                `startDate`_             Not required in StationXML            
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -------------------------------------------------------------
  end_date                          date                           Y           *None*                `endDate`_               Not required in StationXML                
======================== ================================== ============ ================= =============================== =============================================================


 
==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/network.schema.json>`_

==============================
Example
==============================

file: `_templates/TEMPLATE.network.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.network.yaml
  :language: YAML

===================
Class Navigation
===================

:ref:`Network` \<\=\=

