
.. _Subnetwork:

***************
 Subnetwork
***************

===============
 Description
===============

An OBS subnetwork is all or part of an FDSN network of 
:ref:`stations <Station>` in a given campaign.

---------------------
Python class:
---------------------

SubNetwork

---------------------
 YAML / JSON label:
---------------------

subnetwork
  Contained in a subnetwork file.

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Network

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

*None*

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Gathers one or more :ref:`Stations <Station>`
* Is part of a Campaign (not implemented in *obsinfo* as a class).

==============================
Attributes
==============================

.. _StationFDSN: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#station
.. _OperatorFDSN: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#operator
.. _Comment: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#comment
.. _startDate: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#network-required
.. _endDate: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#network-required

======================== ================================== ============= ================= ================================== ================================================
        **Name**                   **Type**                 **Required**    **Default**        **Equivalent StationXML**         **Remarks**
------------------------ ---------------------------------- ------------- ----------------- ---------------------------------- ------------------------------------------------
  subnetwork                    :ref:`FDSNNetwork`                  Y           *None*            *None*                    
------------------------ ---------------------------------- ------------- ----------------- ---------------------------------- ------------------------------------------------
  operator                      :ref:`Operator`                     Y           *None*            `OperatorFDSN`_                       Not required in StationXML  
------------------------ ---------------------------------- ------------- ----------------- ---------------------------------- ------------------------------------------------
  stations                   Array of :ref:`Station`                Y           *None*            `StationFDSN`_                    
------------------------ ---------------------------------- ------------- ----------------- ---------------------------------- ------------------------------------------------
  restricted_state         List of values:                          N           *None*            *None* 
                           "open", "closed", 
                           "partial", "unknown"
======================== ================================== ============= ================= ================================== ================================================

 
==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/subnetwork.schema.json>`_

==============================
Example
==============================

file: `_templates/subnetwork.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.subnetwork.yaml
  :language: YAML



===================
Class Navigation
===================

\=\=\> :ref:`Station`

\=\=\> :ref:`FDSNNetwork`

\=\=\> :ref:`Operator`
