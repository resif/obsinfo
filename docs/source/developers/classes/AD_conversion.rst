.. _ADConversion11:


****************
ADConversion
****************

===============
 Description
===============

Analog-to-Digital conversion stage.  Allows the user to specify the voltage and
digital unit limits, calculates output gain, which ``obsinfo`` will compare
with the specificied gain.

---------------------
Python class:
---------------------

ADConversion

---------------------
 YAML / JSON label:
---------------------

ADCONVERSION

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Coefficients (with no coefficients except one numerator equal to one)

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`Coefficients <Coefficients11>`

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Is nested in :ref:`Stage <Stage>`

==============================
Attributes
==============================

======================== ================================== ============ ================= ========================== =========================================
        **Name**                   **Type**                 **Required**    **Default**    **Equivalent StationXML**                   **Remarks**
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
delay.samples             number                             N            0                *None*
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
v_minus                   number                             Y           *None*            *None*                      minimum voltage input                                                                     
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
counts_minus              integer or hexadecimal string      Y           *None*            *None*                      output counts at minimum voltage input                                                                     
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
v_plus                    number                             Y           *None*            *None*                      maximum voltage input                                                                     
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
counts_plus               integer or hexadecimal string      Y           *None*            *None*                      output counts at maximum voltage input                                                                     
------------------------ ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
counts_dtype              string                             Y           *None*            *None*                      the dtype for counts.  Can be 'int16',
                                                                                                                       'int24', 'int32', 'uint16', 'unit24'                                                                   
                                                                                                                       or 'unit32'.  Critical for interpreting
                                                                                                                       hexadecimal strings ('intXX' values
                                                                                                                       are interpreted as two's complement)                                                                 
======================== ================================== ============ ================= ========================== =========================================

==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/filter.schema.json>`_

==============================
Example
==============================

`_templates/AD_Conversion.filter.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.AD_Conversion.filter.yaml
  :language: YAML

==================
Class Navigation
==================

:ref:`Filter11` \<\=\= 

