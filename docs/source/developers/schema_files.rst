*********************
Schema files
*********************

- Each file corresponds to a possible information file (with 1-2 exceptions)
- Do not use `allOf` or `anyOf` in schema files, unless the things they are
  comparing/combining are very short.  Any failure just prints out everything
  downstream and says that couldn't find a match to `allOf` or `anyOf`, which
  is about useless for validating information files.