.. _information_files:

*****************
Information files
*****************

Here is an explanation of information files, and 
commented templates for each type. 
You can also look at the schemas at :ref:`Schemas`

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  information_files/explanation
  information_files/templates
