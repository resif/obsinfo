.. _subnetwork_examples:

************************
Example subnetwork files
************************

Examples of ``subnetwork`` files, from the most basic to the most
complete.

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  examples_subnetwork/1_basic_flat
  examples_subnetwork/2_basic_atomic
  examples_subnetwork/3_basic_configuration
