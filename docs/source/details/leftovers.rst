*******************************
Leftovers
*******************************

This file contains scraps that were removed from other files when transitioning
to v1.0

subnetwork
_____________________________________

The next field key, ``subnetwork``, starts the actual information.
You may specify several sub-elements which are listed in :ref:`subnetwork`,
but let's stick to the fundamentals:

.. code-block:: yaml

 subnetwork:
    network:
        code: "4G" # Either an FDSN provided network code or "XX" if no such code exists.
        name: "Short period OBSs" # Use the FDSN name if applicable
        start_date: "2007-07-01"
        end_date: "2025-12-31"
        description: "Short period OBS network example"
        operators: [{$ref: "operators/EMSO-AZORES.operator.yaml#operator"}]
    operators:
        -   {$ref: "operators/INSU-IPGP.operator.yaml#operator"}

The ``network`` section describes the network, of which the ``subnetwork`` is
a subset.

If the network has been declared to  FDSN, the information in ``network``
should correspond to the values on the FDSN site.
For information on how to request a network code or use a temporary code,
see this `link <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/network-codes.html>`_ .


stations
_____________________________________

Stations belonging to the network are specified next.
They could, of coursem be put in a different file, but it is a best practice
to put the station information, so that one file contains all of the essential
deployment information.

The following attribute is, therefore, ``stations``.
One can specify as many stations as you want, but in this example we will only specify one.
Stations are identified by a one to five character code.
This code acts like a key, but be careful to surround it by quotes: otherwise
it will be flagged as an error by the JSON syntax validator.
The start_date and end_date should correspond to data start and end.
The site is described as a text field, and a location code is specified too.
More on locations later.

.. code-block:: yaml

   stations:
        "LSVW":
            site: "Lucky Strike Volcano West"
            start_date: "2015-04-22T12:00:00Z"
            end_date: "2016-05-28T21:01:00Z"
            location_code: "00"
            instrumentation:

                ...

instrumentation
_____________________________________

Stations must have an instrumentation, which specifies the entire data recording
system, from the sensor(s) to the datalogger.
The best practice is to specify these in a separate file, as the same instrumentation
(with different serial numbers and possibly different configurations) may be
used at several stations and/or for several experiments
.
The way to reference an instrumentation is the following:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentations/SPOBS2.instrumentation_base.yaml#instrumentation_base"}
        datalogger_configuration: "250 sps"

``$ref`` is a different file in a folder called ``instrumentation``.
It is possible to specify several instrumentations in list format:

.. code-block:: yaml

    instrumentations:
            - {base: {$ref: "instrumentations/SPOBS2.instrumentation_base.yaml#instrumentation_base"}}
            - {$ref: {"instrumentations/BBOBS1_2012+.instrumentation_base.yaml#instrumentation_base"}}

Instrumentations can be configured in several ways.  One almost always provides
a ``datalogger_configuration``, to specify the sample rate and possibly digital
filters and/or gains.  Here is an example using all the possible keys:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentations/SPOBS2.instrumentation_base.yaml#instrumentation_base"}
        serial_number: "01"
        configuration: "low power"
        modifications:
            ...
        channel_modifications:
            ...

The ``serial_number`` key lets you specify the instrumentation's serial number
The ``configuration`` key lets you select a configuration that has been pre-defined
for the instrumentation.
The ``modifications`` and ``channel_modifications`` keys let you modify individual
elements within the instrumentation: we will discuss them later.

Locations
_____________________________________

There must be at least one location in the file.
This is the position in geographical coordinates of the station, usually
referred as location "00".
Locations are specified as follows:

.. code-block:: yaml

    locations:
        "00":
            base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
            configuration: "ACOUSTIC_SURVEY"
            position: {lon.deg: -32.32504, lat.deg: 37.29744, elev.m: -2030}

There is a base, which is again a referenced file in a different folder (as best practice).
Again, there is a ``configuration``, which lets us chose which method was used to
locate the station (a default value is chosen if ``configuration`` is not provided).

Observe the difference between a list and a dictionary.
List items are separated by dashes, dictionaries need a key/value pair.
authors is a list.
locations is a dictionary.


However, there can be several locations.
That's the reason we have a ``location_code`` to specify the location corresponding to the station itself.
Other locations can be used if different channels have different positions or if
two channels have the same FDSN channel code, as will be seen shortly.

In all *obsinfo* information files, you can add ``notes`` as a list of strings.
Notes are not put into the StationXML file, they only serve documentation
purposes within the information file.

``comments`` are added to StationXML files, and
can only be placed at levels which correspond to the levels in a StationXML file
with a ``Comment`` field.

``extras`` are key:value pairs for information that you wish to document/process
but do not correspond to existing obsinfo elements.
They are added as comments to the output StationXML file.
