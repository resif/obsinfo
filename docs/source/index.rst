.. obsinfo-test documentation master file, created by
   sphinx-quickstart on Mon Jul 19 11:50:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Obsinfo documentation
========================================

**obsinfo** is a system for creating StationXML files
including marine seismometer/hydrophone information as
simply as possible.  The basic file is a subnetwork file, which
contains only pertinent deployment information:

Here is a basic subnetwork file with one station.

.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_essential.subnetwork.yaml
    :language: yaml

You can specify as many ``stations`` as you have in
your deployment. The optional ``processing`` section
allows you to specify instrument clock drifts.


.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  :glob:
  
  overview
  installation
  command_line_tools
  subnetwork
  instrumentation
  tutorial
  details
  training_course
  changelog
  developers
  

# Indices and tables
# ==================
# 
# * :ref:`genindex`
# * :ref:`modindex`
# * :ref:`search`
