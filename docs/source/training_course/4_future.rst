.. _training_course_5:

**********************************************
Day 4: Future plans
**********************************************

The following is a list of possible changes for a future version:
  They currently are v1.0 additions that have not yet been implemented

-----------------------------------------------
Language additions
-----------------------------------------------

- non-linear drift processing element
    - decide format
- Add option for GFZ-style permanent references?
     - <gfz:Identifier type='hdl'>10881/sensor.a7561d1a-d518-475d-9733-30370432996c</gfz:Identifier>")
    - Are ”identifiers” (in station, network and channel) and “resource_id” (in Equipment) sufficient?
- allow ``calibration_dates`` in ``stage`` as well as in ``equipment``?
    - stage files are where the changed parameters are kept
    - Could cause confusion/duplication.  Also, how would we handle multi-stage
      calibrations? (dataloggers, ...)
- Add filter shortcuts?
    - Geophone, IIR, SYNC, RC ...

-----------------------------------------------
Language changes
-----------------------------------------------
- Make StationXML relation clearer (but break AROL compatibility)
    - StationXML element names => CamelCase?
    - Use FilterType as key rather than ”filter” + key?
        - Could simplify code

-----------------------------------------------
Other
-----------------------------------------------

- New command-line tools
    - ``obsinfo configurations``:  Print out all configurations available in 
      a file, a directory, or a directory and its subdirectories
    - ``obsinfo plot``: plot station maps or instrument responses
        - instrumentation and below: frequency responses and impulse responses
        - subnetwork: station map, possibly with one frequency response per instrumentation type?
        - person, operator, network, timing, location: nothing
    - ``obsinfo arol``: make AROL libraries from instrumentation database.
    - ``obsinfo nrl``: make NRL libraries from instrumentation database.
        
- Documentation
    - Add information file reference (imitate StationXML Reference)
