.. _training_course_instrumentation:

****************************
The instrumentation database
****************************

The instrumentation database contains the instrumentations belonging to an OBS
facility, as well as all of that instrumentation's components:

- ``channels``, composed of ``instruments`` and ``orientations``
- ``instruments``, which are composed of a ``datalogger``, a ``sensor`` and
  possibly a ``preamplifier``
- ``dataloggers``, ``sensors`` and ``preamplifiers``, composed of ``stages``
- ``stages``, each containing a ``filter``

This document briefly describes these elements and their
relations.  It also briefly describes how to set up ``configurations``.  More
detailed descriptions can be found in the Tutorial sections on

- :ref:`Instrumentation files <Tutorial4>`
- :ref:`Instrument Components <Tutorial5>`
- :ref:`Datalogger files <Tutorial6>`
- :ref:`Stage files <Tutorial7>`
- :ref:`Filter files <Tutorial8>`

----------------------------
header elements
----------------------------
Every obsinfo_ file should have top-level ``format_version`` and ``revision``
elements.
The can also have a ``notes`` element, to write down information that will
NOT be put in the StationXML file.  There may also a top-level field called
``yaml_anchors``. In previous versions of obsinfo_ this was a convenient place to store
repeating elements, but with the current JSONRef system there is less
and less use for this.
We have kept it in case others find it useful.

In most information file examples, we have removed the ``format_version`` and
``revision`` elements to save space, but they should always be there.
 
----------------------------
instrumentation_base files:
----------------------------

See also :ref:`Tutorial4`

``instrumentation_base`` files specify complete instrumentations (ocean bottom
seismometers or hydrophomes, water-column hydrophones, etc.).
A typical file (without configurations) looks like:

.. code-block:: yaml

    instrumentation_base:
        equipment:
            model: "SPOBS2"
            type: "Short Period Ocean Bottom Seismometer"
            description: "LCHEAPO 2000 SPOBS, 4 channels, L-28 3C geophone"
            manufacturer: "Scripps Inst. Oceanography"
            vendor: "Scripps Inst. Oceanography"   
        channels:
            default:
                datalogger: {base: {$ref: "dataloggers/LC2000.datalogger_base.yaml#datalogger_base"}}
                preamplifier: {base: {$ref: "preamplifiers/LCHEAPO_GEOPHONE.preamplifier_base.yaml#preamplifier_base"},
                               configuration: "128x gain"}
                sensor: {base: {$ref: "sensors/SERCEL_L28.sensor_base.yaml#sensor_base"}}
            "1":
                orientation : {"H": {azimuth.deg: {value: 0}, dip.deg: {value: 90}}}
                replace_preamplifier: {base: {$ref: "preamplifiers/LCHEAPO_HYDRO.preamplifier_base.yaml#preamplifier_base"},
                                configuration: "16x gain"}
                replace_sensor: {base: {$ref: "sensors/HITECH_HTI90U.sensor_base.yaml#sensor_base"}}
            
            "2": {orientation: {"2": {azimuth.deg: {value: 90, uncertainty: 180}}}}
            "3": {orientation: {"1": {azimuth.deg: {value: 0, uncertainty: 180}}}}
            "4": {orientation: {"3": {dip.deg: {value: 90}}}}

Whereas a file with configurations looks like:

.. code-block:: yaml

    instrumentation_base:
        equipment:
            model: "BBOBS1"
            type: "Broadband Ocean Bottom Seismometer"
            description: "LCHEAPO 2000 BBOBS 2012-present" 
            manufacturer: "Scripps Inst. Oceanography - INSU"
            vendor: "Scripps Inst. Oceanography - INSU"
        channels:
            default:
                datalogger: {base: {$ref: "dataloggers/LC2000.datalogger_base.yaml#datalogger_base"}}
                preamplifier: {base: {$ref: "preamplifiers/LCHEAPO_BBOBS.preamplifier_base.yaml#preamplifier_base"},
                               configuration: "0.225x gain"}
                sensor: {base: {$ref: "sensors/NANOMETRICS_T240__theoretical.sensor_base.yaml#sensor_base"},
                         configuration: "SN1-399, single-sided"}  
            "1":
                orientation: {"2": {azimuth.deg: {value: 90, uncertainty: 180}}}
            "2":
                orientation: {"1": {azimuth.deg: {value: 90, uncertainty: 180}}}
            "3": 
                orientation: {"Z":  {azimuth.deg: {value: 0}, dip.deg: {value: -90}}}
                preamplifier: {configuration: "1x gain"}
            "4":
                orientation : {"H": {azimuth.deg: {value: 0}, dip.deg: {value: 90}}}
                replace_preamplifier: {base: {$ref: "preamplifiers/LCHEAPO_DPG.preamplifier_base.yaml#preamplifier_base"}}
                replace_sensor: {base: {$ref: "sensors/SIO_DPG.sensor_base.yaml#sensor_base"}}         
                extras:
                    DBIRD_response_type : "CALIBRATED"   
        configurations:
            "SN01":
                equipment: {serial_number: '01'}
                channels:
                    default: {sensor: {configuration: "Sphere01"}}
                    "4": {sensor: {configuration: "generic"}}
            "SN02":
                equipment: {serial_number: '02'}
                channels:
                    default: {sensor: {configuration: "Sphere02"}}
                    "4": {sensor: {configuration: "generic"}} 
            "SN03":
                equipment: {serial_number: '03'}
                channels:
                    default: {sensor: {configuration: "Sphere03"}}
                    "4": {sensor: {configuration: "generic"}}

instrumentation-level configurations are useful when each instrumentation has
specific sensors mounted on them by default.
Note that in the above example there is no ``configuration_default``,
meaning that this ``instrumentation`` must always be specified with a ``configuration``

----------------------------
Instrument_Component files
----------------------------

See also :ref:`Tutorial5`

The next level down from ``instrumentation_base`` files are
``datalogger_base``, ``sensor_base`` and ``preamplifier_base`` files,
collectively refered to as ``instrument_components``.
Thre three types of ``instrument_components`` have several common elements,
most notably ``stages``.
``datalogger`` and ``sensor`` have a couple of specific elements.

datalogger_base files
*******************************

See also :ref:`Tutorial6`

Specific elements
    ``sample_rate`` and ``correction``.

.. code-block:: yaml

    datalogger_base:
        equipment:
            model: "CS5321/22"
            type: "delta-sigma A/D converter + digital filter"
            description: "CS5321/22 delta-sigma A/D converter + FIR digital filter"
            manufacturer: "Cirrus Logic"
            vendor: "various"
        stages:
            - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}
        sample_rate: 125
        correction: 0.232
        configuration_default: "125sps"
        configurations:
            "62.5sps":
                configuration_description: "62.5 sps"
                sample_rate: 62.5
                correction: 0.464
                stages:
                    - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}
            "500sps":
                configuration_description: "500 sps"
                sample_rate: 500
                correction: 0.058
                stages:
                    - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}
            "1000sps":
                configuration_description: "1000 sps"
                sample_rate: 1000
                correction: 0.029
                stages:
                    - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}

This file sets ``configuration_default: "125 sps"`` so, if ``configuration``
is not specified, "125 sps" will be used.

sensor_base files
*******************************

Specific element
    ``seed_codes``

.. code-block:: yaml

    sensor_base:
        equipment:
            model: "Trillium T240"
            type: "Broadband seismometer"
            description: "Trillium T240 seismometer, negative shorted to ground"
            manufacturer: "Nanometrics, Inc"
            vendor: "Nanometrics, Inc"
        seed_codes:
            band_base: "B"
            instrument: "H" 
        configuration_default: "SN1-399, single-sided"
        stages:
            - {base: {$ref: "sensors/stages/Trillium_T240__theoretical.stage_base.yaml#stage_base"}}
        configurations:
            "Sphere01":
                equipment: {serial_number: "133"}
                stage_modifications:
                    '*': {configuration: "SN1-399, single-sided"}
            "Sphere02":
                equipment: {serial_number: "132"}
                stage_modifications:
                    '*': {configuration: "SN1-399, single-sided"}
            "Sphere03":
                equipment: {serial_number: "134"}
                stage_modifications:
                    '*': {configuration: "SN1-399, single-sided"}
            "Sphere06":
                equipment: {serial_number: "830"}
                stage_modifications:
                    '*': {configuration: "SN400+, single-sided"}

The configurations specify the sensor serial number and seismometer response
for each seismometer sphere.  

preamplifier_base files
*******************************

specific elements
    **None**

.. code-block:: yaml

    preamplifier_base:
        equipment:
            model: "BBOBS-GAIN"
            type: "Analog gain card"
            description: "INSU BBOBS gain card"
            manufacturer: "SIO or IPGP"
            vendor: "LCHEAPO"
        stages:
            - {base: {$ref: "preamplifiers/stages/INSU_BBOBS_theoretical.stage_base.yaml#stage_base"}}
        configuration_default: "1x gain"
        configurations:
            "0.225x gain":
                stage_modifications:
                    '*': {configuration: "0.225x"}
            "1x gain":
                stage_modifications:
                    '*': {configuration: "1.0x"}

Preamplifiers are optional.  The configurations specify the analog gain between the sensor
and the datalogger.

stage_base files
*******************************

See also :ref:`Tutorial7`

.. code-block:: yaml

    stage_base:
        description : "SENSOR - TRILLIUM 240 SN1-399 (SINGLE-SIDED)"
        input_units :  {name: "m/s", description: "VELOCITY"}
        output_units : {name: "V", description: "VOLTS"}
        gain : 
            value: 598.45
            frequency: 1.
        filter:
            $ref: "sensors/stages/filters/Trillium_T240_SN1-399_generic.filter.yaml#filter"
        configuration_default: "SN1-399, single-sided"
        configurations:
            "SN1-399, single-sided":
                filter:
                    $ref: "sensors/stages/filters/Trillium_T240_SN1-399_generic.filter.yaml#filter"
                gain: {value: 598.45, frequency: 1.}
            "SN400+, single-sided":
                gain: {value: 594.5, frequency: 1.}
                filter:
                    $ref: "sensors/stages/filters/Trillium_T240_SN400-_generic.filter.yaml#filter"
            "SN1-399, differential":
                filter:
                    $ref: "sensors/stages/filters/Trillium_T240_SN1-399_generic.filter.yaml#filter"
                gain: {value: 1196.9, frequency: 1.}
            "SN400+, differential":
                gain: {value: 1189., frequency: 1.}
                filter:
                    $ref: "sensors/stages/filters/Trillium_T240_SN400-_generic.filter.yaml#filter"

The configurations specify different instrument responses depending on the seismometer
serial number and the connection (single-sided or differential)


filter files:
*******************************

See also :ref:`Tutorial8`

The 5 StationXML filter types can be specified:
``PoleZeros``, ``FIR``, ``Coefficients``, ``ResponseList`` and
``Polynomial``.

obsinfo_ also allows 3 other types for simplicity:

:``ANALOG``:
    An analog gain stage with no filtering (translated to StationXML
    PoleZero with no poles or zeros)
:``ADCONVERSION``:
    Like the Analog stage, but with information about input voltage
    and output count limits
:``DIGITAL``:
    A digital stage with no filtering (translated to StationXML
    Coefficients stage with numerator = [1] and denominator = [])
  
Below is an example pole-zero file.
    
.. code-block:: yaml

    filter:
        type: "PolesZeros"
        transfer_function_type: "LAPLACE (RADIANS/SECOND)"
        zeros:
           - '0.0 + 0.0j'
           - '0.0 + 0.0j'
        poles:
           - '19.99 + 19.99j'
           - '19.99 - 19.99j'

----------------------------
delay and correction
----------------------------

obsinfo_ differs slightly from StationXML_ is in its handling of delays
in digital filters.
StationXML_ (and RESP) have three parameters in each stage, relating to the
time delay created by the stage, in each Stage's Decimation_ section:

.. _Decimation: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#decimation

:Offset:
    Sample offset chosen for use. If the first sample is used, set this field to zero.
    If the second sample, set it to 1, and so forth.
:Delay:
    The estimated pure delay for the stage (in seconds).
    This value will almost always be positive to indicate a delayed signal.
:Correction:
    The time shift, if any, applied to correct for the delay at this stage.
    The sign convention used is opposite the <Delay> value; a positive sign
    here indicates that the trace was corrected to an earlier time to cancel
    the delay caused by the stage and indicated in the <Delay> element. 

A digital filter's true delay is in samples, not seconds, meaning that the
**delay** will depend on the sampling rate.

obsinfo_'s atomic philosphy does not permit a variable delay (in
seconds) when the delay is constant (in samples).
obsinfo_ therefore provides the elements ``delay.samples`` for digital filters
and ``delay.seconds`` for analog filters.
obsinfo_ converts ``delay.samples`` to ``Delay`` (in seconds)
in the output StationXML file.

obsinfo_ does not provide an ``offset`` element because it was not clear to
what it represents, compared to delay.
``offset`` is in samples rather than in seconds and at first we thought that it
was the right way to express digital filter delays, but the StationXML documentation
states that ``offset`` should be between 0 and the decimation factor.

.. _obsinfo: https://gitlab.com/resif/obsinfo
.. _StationXML: http://docs.fdsn.org/projects/stationxml
