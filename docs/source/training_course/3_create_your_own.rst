.. _training_course_2:

***********************************************************************
Day 3: Create a StationXML instrumentation database
***********************************************************************

In each case, use:

- associated example and schema files to help you create the file(s)
- ``obsinfo validate`` to validate your file(s)
- ``obsinfo print`` for something?
- ``obsinfo xml`` to verify that you can create a StationXML file

Create a subnetwork file (using the example instrumentations)
===============================================================
Create your own subnetwork file, using existing instrumentations in the example_directory.

:First-level associated files:
    ``subnetwork``

:Second-level associated files:
    ``person``, ``network``, ``operators``, ``location_bases``, ``timing_bases``

Add your own sensor/datalogger/analog filter
===============================================================
Add and validate sensor, datalogger and/or preamplifier(s) in the example_directory.
Modify an example instrumentation file to reference the new component(s) and
create a StationXML file

:First-level associated files:
    ``datalogger_base``, ``sensor_base``, ``preamplifier_base``,
    ``stage_base``, ``filter``
    
:Second-level associated files:
    ``person``

Add your own instrumentation
===============================================================
Create a new instrumentation (OBS), using your newly created components and
any other specific information

:First-level associated files: ``instrumentation``
:Second-level associated files: ``person``, ``operators``

Putting it all together
===============================================================
Create a subnetwork file corresponding to a deployment of your own
instrumentation.
